<?php
/*
Plugin Name: PageLines Store Developer API
Plugin URI: http://pagelines-developers.com/
Description: 
Author: Evan Mattson
Author URI: 
Version: 1.0.1
pagelines: true
*/

class PLStoreDevAPI {

	const version = '1.0.1';

	private $creds;

	// Construct
	function __construct( $username = '', $apikey = '', $cache = true ) {

		$this->id  = strtolower(__CLASS__);
		$this->url = sprintf('%s/%s', WP_PLUGIN_URL, $this->id);
		$this->dir = sprintf('%s/%s', WP_PLUGIN_DIR, $this->id);

		$this->creds = array(
			'user'     => $username,
			'key'      => $apikey,
		);
		$this->mode     = ($this->creds['user'] && $this->creds['key']) ? 'authenticated' : 'public';
		$this->products = array();
		$this->cache    = $cache ? true : false;

		$this->api = array(
			'dataurl'  => 'http://api.pagelines.com/devs',
			'version'  => '1.1',
			'endpoint' => ''
		);

		$this->actions();
	}

	function actions() {

		// Setup Products
		if ( 'authenticated' == $this->mode )
		add_action( 'init',						array(&$this, 'setup_products') );

		// Custom option
		add_action( 'pagelines_options_apikey', array(&$this, 'apikey_option'), 10, 2 );
	}

	public function request( $request, $args = array() ) {
		return $this->api_request( $request, $args );
	}

	/**
	 * Submits a request to the API
	 *
	 * @uses  $this->is_private_request() 	checks to see if the request requires validation
	 * @uses  $this->return_response() 		determines what to return on success
	 * 
	 * @param  string 	$request 	request slug
	 * @param  array  	$args    	request modifiers/options
	 * @return mixed 				array on success (see return_response method)
	 *                         		string html error messages on error
	 */
	private function api_request( $request, $args = array() ) {

		$d = array(
			'request' => $request,
			'product' => '',
			'days'    => ''
		);

		$args = wp_parse_args( $args, $d );

		// only extract keys defined in defaults
		extract( shortcode_atts( $d, $args ) );
		extract( $this->api );
		extract( $this->creds );

		switch ( $request ) {
			
			case 'developer-total':
				$endpoint = sprintf('all%s', $days ? ":$days" : '');
				break;
			
			case 'product-total':
				$endpoint = sprintf('sales:%s', $days ? "$product,$days" : $product);
				break;
			
			case 'list-products':
				$endpoint = 'list';
				break;

			case 'some-public-request':
				// nothing happening here yet
				break;
		}

		$cached = $this->get_cached_request( $request, $args );

		// use cached?
		if ( $this->cache && is_array( $cached ) ) {
			$args['cached'] = true;
			return $this->return_response( $cached, $args );
		}

		//	at this point the cached data has expired
		//	or nothing has been cached for this request yet
		//  make a new request to the api
		$api_request = "$dataurl/$version";
		$api_request .= $this->is_private_request( $request ) ? "/$user/$key" : '';
		$api_request .= "/$endpoint";

		// send the request
		$response = wp_remote_get( esc_url( $api_request ) );

		return $this->return_response( $response, $args );
	}

	private function return_response( $response, $args ) {

		$d = array(
			'full'    => '',
			'cached'  => '',
			'request' => '',
			'output'  => ''
		);
		extract( shortcode_atts( $d, $args ) );

		// check the response
		if ( is_wp_error( $response ) ) {

			$msg = '<p><strong>Error</strong></p>';
			foreach ( $response->errors as $errors )
				$msg .= sprintf('<p>%s</p>', implode(' ', $errors));

			return $msg;
		}
		
		$body = json_decode( $response['body'], true );

		// to cache or not to cache?
		// make sure we don't recache a cached response
		if (
			!$cached
			&& is_array( $body )
			&& isset( $response['response']['code'] )
			&& 200 === $response['response']['code']
			)
			$this->cache_response( $request, $args, $response );

		// determine what to return
		$r = $full ? $response : $body;

		if ( 'raw' == $output )
			return $r;
		
		$r['date'] = $response['headers']['date'];
		$r['timestamp'] = $this->get_timestamp( $r['date'] );
		$r['localtime'] = $this->get_timestamp( $r['timestamp'], true );

		// tag the returned data as cached 
		$r['cached'] = $cached ? true : false;

		return $r;
	}

	/**
	 * Returns a cached request
	 * Clears cache when caching is disabled
	 * 
	 * @param  string 	$request 	api request type slug
	 * @param  array 	$args    	request args
	 * @return mixed 				Value of transient. If the transient does not exist, does not have a value, or has expired, then get_transient will return false
	 */	
	private function get_cached_request( $request, $args ) {

		$key = $this->get_cached_key( $request, $args );

		if ( !$this->cache )
			delete_transient( $key );

		return get_transient( $key );
	}

	private function cache_response( $request, $args, $data ) {

		$key = $this->get_cached_key( $request, $args );

		if ( $key && $this->cache )
			set_transient( $key, $data, HOUR_IN_SECONDS*2 );
	}

	/**
	 * Assembles an option key for saving/retreiving request data based on the request
	 * 
	 * @param  string 	$request 	api request type slug
	 * @param  array 	$args    	request args
	 * @return string          		key
	 */
	private function get_cached_key( $request, $args ) {

		$user    = $this->creds['user'];
		$product = $args['product'];
		$days    = $args['days'];

		if ( 'product-total' == $request && $product )
			$request .= "_$product"; // append product slug
		
		if ( $days )
			$request .= "_{$days}-days";

		return "{$user}_$request";
	}

	private function is_private_request( $request ) {
		$private = array(
			'developer-total',
			'product-total',
			'list-products'
		);
		return in_array( $request, $private );
	}

	/**
	 * Sets up the 'products' property with an array of user's product data in this format:
	 * array('product-slug' => array(
	 * 		'id'			=> 123,
	 * 		'slug'			=> 'some-product-slug',
	 * 		'name'			=> 'Real Product Name',
	 * 		'description'	=> 'blah blah blah',
	 * 		'lastmod'		=> 1359510914, // timestamp
	 * 		'price'			=> '19.95',
	 * 		'image'			=> 'http://api.pagelines.com/files/plugins/img/some-product-slug-thumb.png',
	 * 		'demo'			=> 'some demo url',
	 * 		'downloads'		=> 321,
	 * 		'type'			=> 'plugin' // plugin/section/theme
	 * 		'sales'			=> array(
	 * 			'total' => 1234,
	 * 			'items' => array(
	 * 				0 => array(
	 * 					'date' => '2013-03-07 00:00:00',
	 * 					'price' => '15.00',
	 * 					),
	 * 				1 => array() // ...	
	 * 				)
	 * 			)
	 *   	),
	 *   	'date' 			=> 'Mon, 25 Mar 2013 03:15:46 GMT',
	 *   	'timestamp' 	=> 1364181346,
	 *   	'localtime' 	=> 1364166946,
	 *   	'cached' 		=> false,
	 * )
	 *
	 * @uses  'list-products' 	api request type
	 * @uses  'product-total' 	api request type
	 */
	function setup_products() {

		$user = $this->creds['user'];

		$vault = get_transient("{$user}_products");

		// check for cached
		if ( $this->cache && is_array($vault) && isset($vault['products']) ) {
			$this->products = $vault['products'];
		}
		else {
			delete_transient("{$user}_products");

			// rebuild the product table!
			$r = $this->api_request( 'list-products', array('full' => 1) );

			$products = array();

			$list = json_decode( $r['body'], true );

			if ( is_array( $list ) ) {

				foreach ( $list as $p ) {
					$p['type'] = self::get_ext_type( $p );
					$p['sales'] = $this->api_request( 'product-total', array('product' => $p['slug'], 'output' => 'raw') );
					$products[ $p['slug'] ] = $p;
				}

				$vault = array(
					'age'      => strtotime( $r['timestamp'] ),
					'products' => $products,
				);
				
				if ( $this->cache )
					set_transient( "{$user}_products", $vault, HOUR_IN_SECONDS*2 );
			}

			$this->products = $products;
		}
	}

	function filter_products( $args ) {

		$d = array(
			'sales_start_date' => '',
			'sales_end_date'   => '',
			'type' => ''
		);
		$args = wp_parse_args( $args, $d );

		if ( empty($this->products) )
			$this->setup_products();

		if ( empty($this->products) )
			return $this->products;
		else
			$all = $this->products;


		$filtered = array();

		foreach ( $all as $slug => $p ) {
			$_prod = $this->filter_product( $p, $args );

			if ( is_array($_prod) )
				$filtered[] = $_prod;
		}

		return $filtered;
	}

	function filter_product( $p, $args ) {

		$type  = str_replace( ' ', '', $args['type'] );
		$types = explode( ',', $type ); // allow multiple types to be specified comma-separated
		
		// allow singular or plural
		foreach ( $types as &$t ) {
			$t = rtrim( $t , 's' );
			$t .= 's';
		}
		// if a type(s) is specified - check to see if there is a match, otherwise bail
		if ( $type && !in_array($p['type'], $types) )
			return false;

		// if filtering sales dates, do it.
		if ( $args['sales_start_date'] || $args['sales_end_date'] )
			$p['sales'] = $this->filter_sales($p['sales'], $args);

		return $p;
	}

	function filter_sales( $sales, $args ) {

		if ( !isset($sales['items']) )
			return $sales;

		$items = $sales['items'];

		// start
		if ( $args['sales_start_date'] )
			$start = $this->get_timestamp( $args['sales_start_date'], true );
		else
			$start = 0;
		// end
		if ( $args['sales_end_date'] )
			$end = $this->get_timestamp( $args['sales_end_date'], true );
		else
			$end = current_time( 'timestamp' );

		// filter sales items
		foreach ( $items as $key => $data ) {
			
			$saletime = $this->get_timestamp( $data['date'], true );

			if ( $start < $saletime && $saletime < $end )
				continue; // match
			else
				unset( $items[ $key ] );
		}

		// rebuild sales
		$newtotal = 0;

		foreach ( $items as $data )
			$newtotal += $data['price'];

		$new_sales = array(
			'total' => $newtotal,
			'items' => array_values( $items ) // reindex
		);

		return $new_sales;
	}

	function get_timestamp( $time, $localize = false ) {
		
		if ( is_int( $time ) || ( strlen(intval($time)) == strlen($time) ) )
			$timestamp = $time;
		else
			$timestamp = strtotime( $time );


		if ( $localize )
			$timestamp += ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
		
		return $timestamp;
	}

	function validate_product( $slug ) {
		return array_key_exists( $slug, $this->products ) ? $slug : '';
	}

	/**
	 * Helper function for retreiving product info
	 * @param  string 	$slug 	product slug
	 * @param  string 	$key  	info key
	 * @return mixed
	 */
	function get_product_info( $slug, $key ) {
		return isset( $this->products[ $slug ][ $key ] ) ? $this->products[ $slug ][ $key ] : '';
	}

	function get_ext_type( $p ) {
		 //'image' => 'http://api.pagelines.com/files/plugins/img/page-less-thumb.png',
		preg_match( '#/files/([^/]+)#', $p['image'], $matches );

		return isset( $matches[1] ) ? $matches[1] : '';
	}

	/**
	 * Custom Option
	 * Works the same as 'text' but displays as input[type=password]
	 */
	function apikey_option( $oid, $o ) {

		echo OptEngine::input_label($o['input_id'], $o['inputlabel']);
		echo OptEngine::input_text($o['input_id'], $o['input_name'], pl_html($o['val']), 'regular-text', 'password', '', $o['placeholder'] );		
	}

} // PLStoreDevAPI